'''
@file Lab4_main.py
@brief This file serves as the program that executes automatically onboard the microprocessor
@author Jacob Lindberg
@date October 22, 2020

This file serves as the main file that lives on board the Nucleo Microprocessor.
This initializes the software serial handshake from the program running on the local machine
to the program that is running on board the microprocessor. This file regularly calls on the
UI_DataGen.py file to obtain the data from the Encoder. 
'''

from pyb import UART
from Lab4_UI_DataGen import Data_Gen
import utime


## Main Class To Run upon Startup of program
class main:
    '''@brief This class serves to run the finite state machine that is found in the UI_DataGen.py file
        imported within the libraries above.
    '''
    
    ## Initialization Function
    def __init__(self):   
        '''
        @param timearray This is the open time array where the time values are contained.
                dataarray This is the open data array where the encoder position values are contained.
                frequency This is the frequency by which the encoder is to take data (Hz)
                i This is the additor for the state machine
        '''
        ## The timestamp for the first iteration
        self.start_time = 0
        ## Current Time
        self.current_time = 0 
        ## Current Time since the program started          
        self.myuart = UART(2)
        ## Develop the Time Array 
        self.timearray = []
        ## Develop the Data Array 
        self.dataarray = []
        ## Specifify The Frequency
        self.frequency = 5
        ## Specify The Additor
        self.i = 0
        
    ## Run Function    
    def run(self):
        ''' 
        This function runs the finite state machine, pulls the encoder values, adds them to a 
        current timestamp, then aggregates this data in a formated string that is then sent over
        the serial communication line attaching the microprocessor to the local machine that
        is powering it via UART connection.
        '''
        
        if self.myuart.any() != 0:  
            val = self.myuart.readchar()
            if val == 71 and self.current_time >= 0:
                self.start_time = utime.ticks_ms()
                self.current_time = utime.ticks_diff(utime.ticks_ms(), self.start_time)  
                ## The While Loop that collects the encoder data until the stop command is pressed or 10 seconds elapses.               
                while (self.current_time/1E3 <= 10):                              
                    if val == 83:
                        task1.Stop()
                        break
                    else:                        
                        if (self.current_time > ((1/self.frequency)*1000*(self.i+1))-10 and self.current_time < ((1/self.frequency)*1000*(self.i+1))+10):
                            encoder_val = task1.Go()
                            self.dataarray.append(encoder_val) 
                            self.timearray.append(self.current_time/1E3)
                            data = '{:},{:}\r\n'.format(self.dataarray[self.i], self.timearray[self.i])
                            self.myuart.write(data)
                            self.i += 1
                        else:
                            pass
                    self.current_time = utime.ticks_diff(utime.ticks_ms(), self.start_time)                              
                    
                    if self.myuart.any():
                        val = self.myuart.readchar()
            else:
                pass
        else:
            pass
## Build the Constructors           
task0 = main()
task1 = Data_Gen()
## Execute the function in a loop that is always on
while True:
    task0.run()
