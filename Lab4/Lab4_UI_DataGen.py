'''
@file Lab4_UI_DataGen.py
@brief This file serves as the finite state machine that does all encoder computation
@author Jacob Lindberg
@date October 22, 2020

This file serves as the backend FSM program to calculate the encoder values based on
the UI interface input from the user. The state machine has 3 states: initialization,
data_collection, and data_termination. These states go in sequential order activated by
the user input

'''


import pyb

class Data_Gen:
    
    '''
    Encoder task.
    
    The object of this class is to call the EncoderDriver class functions to
    regularly pull certain values from the encoder through the motherboard.
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Data Collection
    S1_DATA_COLLECTION  = 1
    
    ## Data Termination
    S2_DATA_TERMINATION = 0

    def __init__(self):
        '''
        Creates a cipher task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        
        '''
        ## Set up Timer on 3
        self.timer = pyb.Timer(3)
        ## Specify the prescaler and the period value for the timer
        self.timer.init(prescaler = 0, period = 0xFFFF)
        ## Define Timer Channel 1 to Pin A6
        self.timer.channel(1, pin = pyb.Pin.cpu.A6, mode = pyb.Timer.ENC_AB)
        ## Define Timer Channel 2 to pin A7
        self.timer.channel(2, pin = pyb.Pin.cpu.A7, mode = pyb.Timer.ENC_AB)
        ## Initialize the encoder counter
        self.timer.counter()
        ## Define the zero position datum
        self.current_pos = self.timer.counter()
        ## Define the First State
        self.state = self.S0_INIT
        
        
        
        #self.val = 0
        
     ## Run Function
    def run(self):
        '''
        Starts the incremental encoder data aggregation onboard the Nucleo
        '''
        
        if (self.timer.counter() >= 65335/2):
            self.delta = self.timer.counter() - 65335/2
            self.current_pos = int(self.delta)
        elif (self.timer.counter() <= 0 and self.timer.counter() <= -65335/2):
            self.delta = abs(self.timer.counter() + 65335/2)
            self.current_pos =int(self.delta)
        elif (self.timer.counter() <= 0 and self.timer.counter() > -65335/2):
            self.delta = abs(self.timer.counter())
            self.current_pos = int(self.delta)
        else:
            self.current_pos = self.timer.counter()
        position = self.current_pos*(360/65536)
        return position
    
    def Go(self):
                   
        if(self.state == self.S0_INIT):
            self.transitionTo(self.S1_DATA_COLLECTION)
            val = self.run()
        elif(self.state == self.S1_DATA_COLLECTION):
            val = self.run()         
            #self.transitionTo(self.S2_DATA_TERMINATION)
        elif(self.state == self.S2_DATA_TERMINATION):
            self.Stop()        
            
        return val   

            
    def Stop(self):
        print("The Program has Terminated")
            
            

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState