'''
@file Lab6_MotorDriver.py
@brief This file containts the motor driver class that operates the PWM cycle to 
        control the movement of the two DC motors.
@author Jacob Lindberg
@date November 12, 2020

'''
import pyb 
## Motor Driver Class
class MotorDriver:
    '''
    This class contains all the functions that are needed to drive the two DC 
    motors with varying PWM inputs using the Nucleo Microcontroller.
    '''
    ## Initialization Function
    def __init__(self):
        '''
        @param nSleep Sleep Pin
        @param DC1_pos Positive Motor1 Terminal
        @param DC1_neg Negative Motor1 Terminal
        @param DC2_pos Positive Motor2 Terminal
        @param DC2_neg Negative Motor2 Terminal
        @param t3 On Board LED Timer
        @param t3 On Board Motor Timer
        @param DC1_pos_PWM Positive Motor1 PWM Signal
        @param DC1_neg_PWM Negative Motor1 PWM Signal
        @param DC2_pos_PWM Positive Motor2 PWM Signal 
        @param DC2_neg_PWM Negative Motor2 PWM Signal
        @param real_LED On Board LED Output Pin
        @param PWM_Percent PWM Duty Cycle 
        '''
        ## Define the nSLEEP pin
        self.nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        ## Set the nSLEEP pin high
        self.nSLEEP.high()
        ## Create the DC1 Motor Positive Pin
        self.DC1_pos = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP) #IN2 Pin
        ## Create the DC1 Motor Negative Pin
        self.DC1_neg = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP) #IN1 Pin
        ## Create the DC2 Motor Positive Pin
        self.DC2_pos = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP) #IN4 Pin
        ## Create the DC2 Motor Negative Pin
        self.DC2_neg = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP) #IN3 Pin
        ## Set the timer for the motor object frequency
        self.t3 = pyb.Timer(3, freq = 20000)
        ## Set the timer for the LED
        self.t1 = pyb.Timer(2, freq=20000)
        ## Define the PWN output for DC1_neg
        self.DC1_neg_PWM = self.t3.channel(1, pyb.Timer.PWM, pin=self.DC1_neg)
        ## Define the PWN output for DC1_pos
        self.DC1_pos_PWM = self.t3.channel(2, pyb.Timer.PWM, pin=self.DC1_pos)
        ## Define the PWN output for DC2_neg
        self.DC2_neg_PWM = self.t3.channel(3, pyb.Timer.PWM, pin=self.DC2_neg)
        ## Define the PWN output for DC2_pos
        self.DC2_pos_PWM = self.t3.channel(4, pyb.Timer.PWM, pin=self.DC2_pos)
        ## Define the onboard LED
        self.real_LED = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        ## Set up the LED Timer
        self.t1ch1 = self.t1.channel(1, pyb.Timer.PWM, pin=self.real_LED)
        ## Define the PWM Duty Cycle percent
        self.PWM_Percent = 50
    ## Run Function for Motor 1   
    def Motor1(self):
        
        ## Turn the motors to the right
        self.t1ch1.pulse_width_percent(self.PWM_Percent)
        self.DC1_pos.low()
        self.DC1_neg_PWM.pulse_width_percent(self.PWM_Percent)
    ## Run Function for Motor 2    
    def Motor2(self):
        
        self.DC2_neg.low()
        self.DC2_pos_PWM.pulse_width_percent(self.PWM_Percent)
        
## Create the constructor        
task1 = MotorDriver()
        
while True:
    ## Task for first motor
    task1.Motor1()
    ## Task for second motor
    task1.Motor2()
    
            
            
        
        