'''
@file Lab6_ClosedLoop.py
@brief Closed Loop File used to compute the pulse width percent that is then 
        passed to the MotorDriver.py file
@author Jacob Lindberg
@date November 24, 2020

This file serves to implement the given equation from Professor Charlie to compute 
the required pulse width percentage needed to make the system reduce the steady 
state error from the actual speed of the motor to the desired speed

'''

import shares
## Closed Loop L% Class
class ClosedLoop:
    
    def __init__(self):
        '''
        @brief initializes the constructor
        '''
    
        self.timer = 10    

    ## Function to compute the pulse width of the signal
    def run(self):
        '''
        @brief runs the calculation to get the percentage of PWM needed
        '''
        L_per = self.get_Kp() * (shares.omega_ref - shares.omega_act)
        ## Upper limit
        if L_per >= 70: 
            L_per = 70
        ## Lower Limit
        elif L_per < 30:
            L_per = 30
        ## Pass
        else:
            pass        
        return L_per  
          
    ## Get_Kp Function   
    def get_Kp(self):
        '''
        @brief Gets the Kp from the user input parameter
        '''
        shares.Kp = shares.Kp_prime / 3.3
        return shares.Kp
        