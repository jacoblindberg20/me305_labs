'''
@file Lab6_Main.py
@brief Main file used to create the constructors for the drivers
@author Jacob Lindberg
@date November 24, 2020
@image html Kp=30.png
@image html Kp=20.png
@image html Kp=10.png
@image html Diagram1.PNG
@image html Calcs.PNG

This file serves to communicate with the front end UI to grab the Kp value
and then pass that into the drivers to start the finite state machine used
to generate the closed loop feedback control system for the motor and the
encoder.
'''
## Import Neccessary Modules
from Lab6_Run import FSM 
from pyb import UART
import shares
## Specify the uart channel
## Call the constructor for the Finite State Machine and pass in how long you would like the program to run for
task1 = FSM(5)
## While loop for executing the function
## Initialize the Class
class start:
    '''
    @brief Class for the main loop
    '''
    ## Initialization Function
    def __init__(self):
        ## Specify Uart
        self.uart = UART(2)
    ## Run Function
    def run(self):
        print("HERE????")
        while True:
            if self.uart.any() != 0:
                val = self.uart.readchar()
                print(val)
                print("This is the value that came in: " + str(val))
                shares.Kp_prime = val.strip()
                shares.prompt = True
                task1.run()
                break         
            else:
                pass
        


## Constructors    
task0 = start()    
task1 = FSM(5)

## Call the Class
task0.run()

