'''
@file shares.py 
@brief file used to connect global variables needed for intermodule communication
@author Jacob Lindberg
@date Novermber 24, 2020

This file stores all data needed for variables that are either pulled or pushed 
globaly by other program files living on the microprocessor

'''
## Inputed Proportional Gain from User
Kp_prime = 30
## Outputed Proportional Gain Calculated
Kp = 0
## Time delta increment in milliseconds
time_delta = 20
## Omega ref (rad/s)
omega_ref = 125
## Omega act (rad/s)
omega_act = 0
## Trigger a response variable
prompt = True