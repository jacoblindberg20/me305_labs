'''
@file Lab7_EncoderDriver.py
@brief Driver Task created to compute the actual speed Omega_act
@author Jacob Lindberg
@date December 2, 2020

This file serves to function as the Encoder Driver task, handling the obtainment
of the encoder position once every 20 miliseconds. The encoder ranges from 0 to hex 0xFFFF
and the delta values are calculated in the Lab7_Run file.

'''
## Import necessary modules
import pyb
## Create the Encoder Driver Class
class EncoderDriver:
    
    '''
    @brief Initialize the EncoderDriver Task
    @param current_pos current position of the encoder
            update updates position of the encoder
            delta the offset delta of the encoder
   '''
    ## Initialization Function
    def __init__(self):
        '''
        @brief Initialize the encoder pins and timer frequency.
        '''
        ## Set Up Tiemr 3
        self.timer = pyb.Timer(8)
        ## Specify the prescaler and the period value for the timer
        self.timer.init(prescaler = 0, period = 65535)
        ## Define Timer Channel 1 to Pin B6
        self.timer.channel(1, pin = pyb.Pin.cpu.C6, mode = pyb.Timer.ENC_AB)
        ## Define Timer Channel 2 to pin B7
        self.timer.channel(2, pin = pyb.Pin.cpu.C7, mode = pyb.Timer.ENC_AB)
        ## Initialize the encoder counter
        self.timer.counter()
        ## Define the delta value
        self.delta = 0
        ## Define the current position 
        self.current_pos = 0
    ## Function to get the current position of the motor    
    def Current(self):
        '''
        @brief This function returns the current position of the encoder in the unit
        of ticks. No delta calculation is computed in this task!
        '''
        ## Current position of the motor in ticks
        current_pos = self.timer.counter()
        return current_pos

        
    
