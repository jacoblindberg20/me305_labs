'''
@file Lab2_mytest.py
@author Jacob Lindberg
@date October 12, 2020

This file serves to function as the dual synchronoys LED finite state machine.
The code within this program will blink a virtual LED simultaneously with one 
that is physically located on the board of the NUCLEO. The LED on the nucleo 
will start with a PWM signal corresponding to a sinusoidal graph. After 30 
seconds the on board LED will gradually become brighter until shutting off and
growing brighter from this state; resembling a sawtooth graph.

'''

import pyb
import math
import utime

## Define the class 
class LED_Blinker:
    
    
    ## Initialization function
    def __init__(self, interval, real_LED):
        
        '''
        @brief Creates the initialization commands upon execution of file
        @param real_LED the LED object used to communicate with the onboard
                LED on the STM32 Nucleo
                
        
        '''
        ## Build the virtual LED object
        self.virtual_LED = False
        ## Set the real LED for the on board system to a global instance
        self.real_LED = real_LED              
        ## The timestamp when the program initializes
        self.start_time = utime.ticks_us() #in microseconds
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval * 1e6)   
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        ## The timestamp that is updated continually since the program started to give current time
        self.curr_time = utime.ticks_diff(utime.ticks_us(), self.start_time)
        ## The first timer object on board the STM32
        self.t1 = pyb.Timer(2, freq = 20000)
        ## The timer channel on  board the STM32
        self.t1ch1 = self.t1.channel(1, pyb.Timer.PWM, pin=self.real_LED)
        
    ## Virtual LED Generator
    def virtual(self):
        '''
        @brief This function serves to operate the virtual LED that will blink 
                on and off virtually apparent by the state in which it is in
                on the REPL terminal ran through PuTTy. I have set it such 
                that the LED is on for one second then off for one second.
        '''
        ## Virtual LED Engine
        if ((int(self.curr_time / 1e6))%2 == 1):
            x = "ON"
        else:
            x = "OFF"
        return x
        
    ## Sinusoidal Wave Function Generator
    def sinusoidal(self):
        '''
        @brief This function is called upon for the first 30 seconds of the 
                program running. The LED will become brighter and then dimmer
                completing one cycle of complete PWM in 10 seconds
        @param duty_cycle this parameter is used to evaluate in real time the
                duty cycle percentage from 0-100 that is being transferred to
                the LED to control its brightness
        '''
        ## This is the built in PWM engine where the values are calculated based off current time
        duty_cycle = abs((int(50*math.sin(.628*(self.curr_time/1e6) + 3.14)) + 50))
        print("The duty cycle for the SIN wave is currently at: " + str(duty_cycle) + ". The Virtual LED is: " + self.virtual())
        return duty_cycle
    
    ## Sawtooth Wave Function Generator
    def sawtooth(self):
        '''
        @brief This function is called upon once 30 seconds from the 
                initialization of this program has been reached. The wave 
                function will start from a duty cycle percentage of 0 and 
                slowly ramp up to 100% once every 10 seconds has elapsed
        @param duty_cycle this parameter is used to evaluate in real time the
                duty cycle percentage from 0-100 that is being transferred to
                the LED to control its brightness
        '''
        
        ## This is the engine that calculates the real time PWM for the sawtooth function
        interv = float((self.curr_time - 30000000) / 1e6)
        if (interv < 10):
            duty_cycle = int(10*interv) 
            print("The duty cycle for the RAMP wave is currently at: " + str(duty_cycle) + ". The Virtual LED is: " + self.virtual())               
        elif (interv < 20 and interv > 10):
            duty_cycle = int(10*(interv - 10)) 
            print("The duty cycle for the RAMP wave is currently at: " + str(duty_cycle) + ". The Virtual LED is: " + self.virtual()) 
        elif (interv < 30 and interv > 20):
            duty_cycle = int(10*(interv - 20))    
            print("The duty cycle for the RAMP wave is currently at: " + str(duty_cycle) + ". The Virtual LED is: " + self.virtual()) 
        elif (interv < 40 and interv > 30):
            duty_cycle = int(10*(interv - 30)) 
            print("The duty cycle for the RAMP wave is currently at: " + str(duty_cycle) + ". The Virtual LED is: " + self.virtual()) 
        else:
            print("The test is now complete!")
        
        return duty_cycle
    
    ## Execution Function
    def run(self):
        
        '''
        @brief      Runs one iteration of the task for the Elevator Movement
        '''
        #print(" This is the current time: " + str(self.curr_time) + "and this is the start time " + str(self.start_time))
        ## If the time elapsed is less than 30 seconds, run the sinusoid function            
        if(self.curr_time < (30000000)):
            self.t1ch1.pulse_width_percent(self.sinusoidal())
        ## If the time has surpassed 30 seconds, run the sawtooth function
        elif(self.curr_time >= (30000000)): 
            self.t1ch1.pulse_width_percent(self.sawtooth())
        else:
            pass
            
        self.curr_time = utime.ticks_diff(utime.ticks_us(), self.start_time)   #updating the current timestamp


       
        
        