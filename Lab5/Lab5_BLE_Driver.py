'''
@file Lab5_BLE_Driver.py
@brief This file serves as the finite state machine that communicates with the HM-19 BLE Module
@author Jacob Lindberg
@date November 11, 2020

This file serves as the backend FSM program to calculate the encoder values based on
the UI interface input from the user. The state machine has 3 states: initialization,
data_collection, and data_termination. These states go in sequential order activated by
the user input

'''


import pyb
import utime


class BLE_Driver:
    
    '''
    Encoder task.
    
    The object of this class is to call the EncoderDriver class functions to
    regularly pull certain values from the encoder through the motherboard.
    '''

    ## Initialization state
    S0_INIT = 1
    
    ## Wait for the Command from the BLE Module
    S1_WAITCMD  = 2
    
    ## Execute the BLE Module Command
    S2_EXECCMD = 3
    ## Initialization Function
    def __init__(self):
        '''
        Creates a cipher task object.
        @param myuart Specify the UART Serial 3 at a Baud Rate of 9600 Bits/s
                real_LED Define the onboard LED on the Nucleo
                state Define the initial state of the program
                start_time Define the start Time
                next_time Define the interval time for the period
                val Define the command sent by the user via BLE       
                interval Define the period for the specified frequency to which the light will blink
        '''
        ## Define the UART to the BLE on Serial 3 with Baud Rate of 9600
        self.myuart = pyb.UART(3, 9600)
        ## Define the Nucleo LED that will be driven by the commands
        self.real_LED = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        ## Define the first initial state of the program
        self.state = self.S0_INIT
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        ## Set the default interval
        self.val = 0
        print("Please connect your IoS device to the HM19 module before starting")
    ## Run Function
    def run(self):
        ''' 
        This program will run the FSM to translate the user input from their bluetooth compatible
        phone into digital commands that can be sent to the onboard LED to execute those commands
        '''

        ## First State Program    
        if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_WAITCMD)   
                
        ## Second State Program    
        elif(self.state == self.S1_WAITCMD):
            if self.myuart.any() != 0:
                self.val = self.myuart.readline()
                self.myuart.write(self.val)
                print("The value that has been received is: " + str(self.val))
                if self.val in range(0,10):
                    self.start_time = utime.ticks.ms()
                    self.transitionTo(self.S2_EXECCMD)
                elif self.val == 'ON':
                    self.LED_ON()
                elif self.val == 'OFF':
                    self.LED_OFF()
                else:
                    print("Please enter a valid argument")                        
            else:
                print("waiting for command...")
                pass
            
        ## Third State Program       
        elif(self.state == self.S2_EXECCMD):
            ##Specify the current time
            self.curr_time = utime.ticks.ms()
            ## Specfify the frequency to which the system will blink
            self.interval = (0.5 * (self.val**-1))
            if self.curr_time <= (self.interval + self.start_time):
               self.LED_ON()
            elif self.curr_time > (self.interval + self.start_time):
               self.LED_OFF()
               if self.curr_time > (2*self.interval + self.start_time):
                   self.start_time = utime.ticks.ms()
               else:
                   pass
            else:
               pass             
    
    ## Transition Function                    
    def transitionTo(self, newstate):
        '''
        This is the transition function that toggles between the states
        '''
        self.state == newstate
        
    ## Constantly Check for New Serial Values
    def CheckVal(self):
        '''
        This is the function that checks whether or not any new values have been
        sent from the bluetooth device to the board
        '''
        if self.myuart.any() != 0:
            self.transitionTo(self.S1_WAITCMD)
        else:
            pass
        
    ## LED On Function
    def LED_ON(self):
        '''
       This is the function that turns the LED ON
       '''
        self.real_LED.high()
        self.CheckVal()
        
    ## LED Off Function
    def LED_OFF(self):
        '''
        This is the function that turns the LED OFF
        '''
        self.real_LED.low()
        self.CheckVal()

 
