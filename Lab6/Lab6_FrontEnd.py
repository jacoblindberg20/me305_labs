'''
@file Lab6_FrontEnd.py
@brief This file serves as the Front End User Interface living on users local machine
@author Jacob Lindberg
@date November 24, 2020

This file serves as the front end graphical user interface for the user to input a 
proportional gain value to then observe the respective transient step response for the
closed loop feedback proportional only control system implemented here. 
'''

import matplotlib.pyplot as plt
import serial
import time
## Set up the serial communication line
ser = serial.Serial(port='COM3', baudrate = 115273, timeout=1)
cmd = ''
## Prompt user for input
while cmd != 'G':
    cmd = input("Please type 'K' to start the system:")
    if cmd == 'K':
        Kp = input('Enter the numerical constant for K: ')
        t = 5
        ## Countdown timer
        while t > 0:
            print("The program will start in: " + str(t))
            time.sleep(1)
            t -= 1
        print("Program has started!" + Kp)
        ser.write('{:}'.format(Kp).encode('ascii'))
        break
    else:
        "Please type correct letter!"
        pass

## Empty Time Array    
t = []
## Empty Speed Array
speed = []
## Initialize the time
start = round(time.time())
## Compute the end time
end = round(time.time()) + 6
## Loop for collecting data and storing it
while round(time.time()) <= end:
    val = ser.readline()
    #print(val)
    #val.strip()
    #data = val.split(',')
    #t.append((data[1]))
    #speed.append((data[0]))
print("The program has terminated")
## PLot the Graph
plt.plot(t, speed, 'r')
plt.ylabel('speed(rad/s)')
plt.xlabel('time(s)')
plt.title('Motor Speed vs. Time')
plt.axis([0, 5, 0, 180])



ser.close()