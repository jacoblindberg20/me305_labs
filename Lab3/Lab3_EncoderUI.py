'''
@file Lab3_EncoderUI.py
@brief This file serves as the user interaction with the motherboard
        in order to recieve values
@author Jacob Lindberg
@date October 20, 2020

This file serves as the User Interface for the user, telling the EncoderDriver task to execute
certain functions relative to the input from the user then understood by the microprocessor.
The UI executes certain commands based on particular ASCII character decimal values associated
with their respective letter combinations on the US standard QWERTY keyboard.

'''

from pyb import UART
from Lab3_EncoderDriver import EncoderDriver


## Define the Encoder User Interface Class
class EncoderUI:
    ## Initialize the Program
    def __init__(self):
        
        self.my_uart = UART(2)
    '''
    @brief This function runs the module for the user interface
    '''
    ## User Interface Generation Function    
    def run_module(self):

        if (self.my_uart.readchar() == 112):
            EncoderDriver.getposition
                            
        elif (self.my_uart.readchar() == 122):
            EncoderDriver.setposition
            #print("The character you have pressed is 'z'")
            
        elif (self.my_uart.readchar() == 100):
            EncoderDriver.getdelta
            #print("The character you have pressed is 'd'")
            
        else:
            pass
        
