'''
@file Lab3_Encoder.py
@brief Encoder FSM used to execute the code required for the interaction with the encoder
@author Jacob Lindberg
@date October 20, 2020

This file serves as the Finite State Machine that was generated to create the encoder task
required of ME305 Lab Students for Lab3.

'''


import utime
from Lab3_EncoderUI import EncoderUI
import Lab3_shares
from pyb import UART
from Lab3_EncoderDriver import EncoderDriver


class Encoder:
    '''
    Encoder task.
    
    The object of this class is to call the EncoderDriver class functions to
    regularly pull certain values from the encoder through the motherboard
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1

    def __init__(self, taskNum, interval):
        '''
        Creates a cipher task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Initialize the Serial Communication Port
        self.my_uart = UART(2)
        
        print("Press enter to start!")
        
        
    '''
    @brief This task serves to run the entire Encoder FSM
    '''
    ## Run Function
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                #self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                if self.my_uart.any():
                    EncoderUI.run_module
                    self.printTrace()    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        EncoderDriver.update_val
        string = ("Commands: ['p'=position; 'z'=zero; 'd'=delta] Current Position: " + str(Lab3_shares.pos))
        print(string)
    
