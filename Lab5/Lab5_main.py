'''
@file Lab5_main.py
@brief This program serves as the hardware interface on the board that initializes
        once the board has been plugged into power
@author Jacob Lindberg
@date November 9, 2020
@image html BLE_FSM.PNG

Here is the link to the thunkable app: 
https://x.thunkable.com/projects/5fac271f9f35d80012421692/a20bdd83-8552-4a11-af4a-e8fcb13d6a37/designer

This program encooperates the functionality of using Bluetooth to control the blinking
of an LED onboard the Nucleo Microcontroller. The system that was designed was built around
the DSD Tech app using the HM-19 BLE 5.0 Module manufactured by DSD Tech. This module, in order
to be discoverable by the app, had to make sure that the EN pin was left floating(neither
attached to ground or the positive rail.) The TX and RX pins were assigned to the Serial3 communication
port on board the Nucleo wth a baud rate of 9600. This program makes use of the BLE_Driver.py 
file that controls the driving of the LED based off the user input via bluetooth commands.
'''

from Lab5_BLE_Driver import BLE_Driver
## Identify the task for this task
task1 = BLE_Driver()
while True:
    task1.run()
    