'''
@file Lab7_MotorDriver.py
@brief This file containts the motor driver class that operates the PWM cycle to 
        control the movement of the two DC motors.
@author Jacob Lindberg
@date December 2, 2020

This file serves to set up the timer, the pins, and the protocols to both initialize and 
opperate the functionality of one of the two motors on the hardware board ME 305 students
were given to create closed loop feedback proportional control from a reference velocity 
profile.

'''
## Import necessary modules
import pyb 
## Motor Driver Class
class MotorDriver:
    '''
    @brief This class contains all the functions that are needed to drive the two DC 
    motors with varying PWM inputs using the Nucleo Microcontroller.
    '''
    ## Initialization Function
    def __init__(self):
        '''
        @brief Initialization of the motor task to get the motor running
        '''
        ## Define the nSLEEP pin
        self.nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        ## Set the nSLEEP pin high
        self.nSLEEP.high()
        ## Create the DC1 Motor Positive Pin
        self.DC1_pos = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP) #IN2 Pin
        ## Create the DC1 Motor Negative Pin
        self.DC1_neg = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP) #IN1 Pin
        ## Create the DC2 Motor Positive Pin
        self.DC2_pos = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP) #IN4 Pin
        ## Create the DC2 Motor Negative Pin
        self.DC2_neg = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP) #IN3 Pin
        ## Set the timer for the motor object frequency
        self.t3 = pyb.Timer(3, freq = 20000)
        ## Define the PWN output for DC1_neg
        self.DC1_neg_PWM = self.t3.channel(1, pyb.Timer.PWM, pin=self.DC1_neg)
        ## Define the PWN output for DC1_pos
        self.DC1_pos_PWM = self.t3.channel(2, pyb.Timer.PWM, pin=self.DC1_pos)
        ## Define the PWN output for DC2_neg
        self.DC2_neg_PWM = self.t3.channel(3, pyb.Timer.PWM, pin=self.DC2_neg)
        ## Define the PWN output for DC2_pos
        self.DC2_pos_PWM = self.t3.channel(4, pyb.Timer.PWM, pin=self.DC2_pos)
        
    ## Run Function for Motor 1  
    '''
    @brief This is the motor task that simply turns the motor that the respective 
    encoder is attached to. In this case, it is Motor 2
    '''
    ## Motor Run Function
    def Motor1_GO(self, PWM):
        '''
        @brief This task turns the motor on from a given duty cycle percentage
        @param PWM this parameter is passed in from being calcualted in the ClosedLoop
        task.
        '''
        ## Spin Motor 2
        self.DC2_pos.low()
        self.DC2_neg_PWM.pulse_width_percent(PWM)
        
    ## Motor Stop Function   
    def Motor1_STOP(self):
        '''
        @brief Stops the motor once the entire task is doen
        '''
        self.nSLEEP.low()
        print("The Motor Has Stopped")
        