'''
@file Lab3_EncoderDriver.py
@brief Driver Task created to call all action regarding the motor
@author Jacob Lindberg
@date October 20, 2020

This file serves to function as the Encoder Driver task, handling the bulk of the computation
in the task, such as computing the delta to then add to the current position as to avoid 
overflow of the encoder values.

'''


import pyb
import Lab3_shares


## Create the Encoder Driver Class
class EncoderDriver:
    
    '''
    @brief Initialize the EncoderDriver Task
    @param current_pos current position of the encoder
            update updates position of the encoder
            delta the offset delta of the encoder
   '''
    
    
    ## Initialization Function
    def __init__(self):
        ## Set Up Tiemr 3
        self.timer = pyb.Timer(3)
        ## Specify the prescaler and the period value for the timer
        self.timer.init(prescaler = 0, period = 0xFFFF)
        ## Define Timer Channel 1 to Pin A6
        self.timer.channel(1, pin = pyb.Pin.cpu.A6, mode = pyb.Timer.ENC_AB)
        ## Define Timer Channel 2 to pin A7
        self.timer.channel(2, pin = pyb.Pin.cpu.A7, mode = pyb.Timer.ENC_AB)
        ## Initialize the encoder counter
        self.timer.counter()
        ## Define the delta value
        self.delta = 0
        ## Define the update value
        self.update = 0
        ## Define the current position
        self.current_pos = 0
    
    '''
    @brief Update value function called upon by the Encoder.py file to repeatedly 
            call the function to get the most recent value of the encoder.
    '''
   
    ## Update Value Function
    def update_val(self):
               
        
        if (self.timer.counter() >= 65335/2):
            self.delta = self.timer.counter() - 65335/2
            self.current_pos = int(self.delta)
        elif (self.timer.counter() <= 0 and self.timer.counter() <= -65335/2):
            self.delta = abs(self.timer.counter() + 65335/2)
            self.current_pos =int(self.delta)
        elif (self.timer.counter() <= 0 and self.timer.counter() > -65335/2):
            self.delta = abs(self.timer.counter())
            self.current_pos = int(self.delta)
        else:
            self.current_pos = self.timer.counter()
        
        Lab3_shares.pos = self.current_pos
        Lab3_shares.delta = self.delta
        return self.current_pos
    
    '''
    @brief The Get Position function grabs the current position of the encoder
    '''
    ## Get Position Function
    def get_position(self):
        self.current_pos = self.update
        Lab3_shares.pos = self.current_pos
        return self.current_pos
    
    '''
    @brief The Set Position is what sets the position of the encoder to 0
    '''
    ## Set Position Function
    def set_position(self):
        ##Position has to be a number between 0-65335
        
        self.current_pos = 0
        Lab3_shares.pos = self.current_pos
        
        return self.current_pos
    
    '''
    @brief The Get Delta function obtains the delta difference from the median
            encoder value to ensure that overflow of the encoder bits does not 
            occur
    '''
    ## Get Delta Function
    def get_delta(self):
        self.delta = abs(self.timer.counter() - self.current_pos)
        Lab3_shares.pos = self.current_pos
        Lab3_shares.delta = self.delta
        return self.delta
            
    
    
    
    
            
        
        
        
        
        
        