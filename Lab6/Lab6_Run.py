'''
@file Lab6_Run.py
@brief This file serves as the back end program for the Finite State Machine
@author Jacob Lindberg
@date November 12, 2020

This file is what links the feedback system together to get the closed loop 
proportional control. This program is also responsible for the timing of the 
system with regards to intervals and such

'''

## Import Neccessary Modules
import utime
import shares 
from Lab6_MotorDriver import MotorDriver
from Lab6_EncoderDriver import EncoderDriver
from Lab6_ClosedLoop import ClosedLoop
from pyb import UART

## Finite State Machine Class
class FSM:
    
    ## State 0
    S0_INIT = 0
    ## State 1
    S1_USERPROMPT = 1
    ## State 2
    S2_MOTOR = 2
    ## State 3
    S3_ENCODER = 3
    ## State 4
    S4_STOP = 4
    
    ## Initialization Function
    def __init__(self, seconds):
        '''
        @brief initializes the program
        @param start_time Start time for the program
        @param seconds The duration of the program specified by the user
        @param end_time The ending time of the program
        @param state The initial state
        @param interval The time delta interval for the program
        @param EncoderPositions Empty encoder positions array
        @param EncoderSpeeds Empty encoder speeds array
        @param TimeStamps Empty time stamps array
        @param EncoderPPR Encoder Pulses Per Revolution Constant
        @param uart Uart serial port
        
        '''
        ## Specify the start time of the program
        self.start_time = utime.ticks_ms()
        ## Localize the duration of the program in milliseconds
        self.miliseconds = seconds * 1000
        ## Specify the end time of the program
        self.end_time = self.start_time + self.miliseconds
        ## Designate the initial state value
        self.state = 0
        ## Interval designated for time step calculations
        self.interval = shares.time_delta
        ## Encoder Position Array (ticks)
        self.EncoderPositions = []
        ## Encoder Speed Array (rad/s)
        self.EncoderSpeeds = [0]
        ## Timestamp array in seconds
        self.TimeStamps = [0]
        ## Pulses per Revolution of the Encoder
        self.EncoderPPR = 4000
        ## Specify the UART Line
        self.uart = UART(2)
        ## Last Encoder Position
        self.EncoderLast = 0
        ## Current Encoder Position
        self.EncoderCurr = 0
        
    
    ## Run Function    
    def run(self):
        '''
        @brief runs the closed loop control
        '''
        
        self.curr_time = utime.ticks_ms()  
        if (self.curr_time <= self.end_time):
            if(self.state == self.S0_INIT): # Initialization Stage
                self.transitionTo(self.S1_USERPROMPT)
            elif(self.state == self.S1_USERPROMPT): 
                if shares.Kp_prime == True:
                    self.transitionTo(self.S2_MOTOR)
                else:
                    pass
            i = 0            
            while i <= self.miliseconds/self.interval:               
                               
                self.curr_time = utime.ticks_ms()
                if (self.curr_time) >= (self.start_time) + (self.interval*i):
                    MotorDriverTask.Motor1_GO(ClosedLoopTask.run())
                    if i == 0:
                        #self.EncoderLast = EncoderDriverTask.Current()
                        self.EncoderPositions.append(EncoderDriverTask.Current())
                        self.TimeStamps.append(0)
                    elif i > 0:
                        #self.EncoderCurr = EncoderDriverTask.Current()
                        self.EncoderPositions.append(EncoderDriverTask.Current()) 
                        #if self.EncoderCurr >= self.EncoderLast:
                        if self.EncoderPositions[i] >= self.EncoderPositions[i-1]:
                            #distance = ((self.EncoderCurr - self.EncoderLast)*((2*3.14)/4000))
                            distance = ((self.EncoderPositions[i] - self.EncoderPositions[i-1])*((2*3.14)/4000))
                        #elif self.EncoderCurr < self.EncoderLast:
                        elif self.EncoderPositions[i] < self.EncoderPositions[i-1]:
                            #distance = ((65535 - self.EncoderLast) + self.EncoderCurr)*((2*3.14)/4000)
                            distance = ((65535 - self.EncoderPositions[i-1]) + self.EncoderPositions[i])*((2*3.14)/4000)
                        shares.omega_act = round(distance/(self.interval/1E3))
                        print("Radians: " + str(distance) + "Encoder Position: " + str(self.EncoderCurr) + " Error: " + (str(shares.omega_ref - shares.omega_act)))
                        self.EncoderSpeeds.append(shares.omega_act)
                        self.TimeStamps.append((self.curr_time - self.start_time)/1E3)
                        print("Current Speed (rad/s): " + str(shares.omega_act) + ". Time elapsed(s): " + str(round((self.curr_time - self.start_time))) + " NUM: " + str(i))
                        self.EncoderLast = self.EncoderCurr
                    data = '{:},{:}\r\n'.format(self.EncoderSpeeds[i], self.TimeStamps[i])
                    self.uart.write(data)
                    i += 1
                else:
                    pass
            
            self.transitionTo(self.S4_STOP)   
            shares.prompt = False   
            MotorDriverTask.Motor1_STOP()
            print(self.EncoderSpeeds)
            print(self.TimeStamps)
            
    ## Transition function
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        
## BUILD THE CONSTRUCTORS
MotorDriverTask = MotorDriver()
EncoderDriverTask = EncoderDriver()
ClosedLoopTask = ClosedLoop()
   
        
        
        