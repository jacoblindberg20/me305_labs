'''
@file Lab7_shares.py 
@brief file used to connect global variables needed for intermodule communication
@author Jacob Lindberg
@date December 2, 2020

This file stores all data needed for variables that are either pulled or pushed 
globaly by other program files living on the microprocessor. Since the serial communication
for my machine was not working, a generated array for the posiiton, speed, and time at an interval
of 200 microseconds was obtained then transfered into their respective variables below

'''
## Inputed Proportional Gain from User
Kp_prime = 7
## Outputed Proportional Gain Calculated
Kp = 0
## Time delta increment in milliseconds
time_delta = 20
## Omega ref (RPM)
omega_ref = 2
## Omega act (RPM)
omega_act = 0
## Trigger a response variable
prompt = True
## Time Array
time = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.2, 5.4, 5.6, 5.8, 6.0, 6.2, 6.4, 6.6, 6.8, 7.0, 7.2, 7.4, 7.6, 7.8, 8.0, 8.2, 8.4, 8.6, 8.8, 9.0, 9.2, 9.4, 9.6, 9.8, 10.0, 10.2, 10.4, 10.6, 10.8, 11.0, 11.2, 11.4, 11.6, 11.8, 12.0, 12.2, 12.4, 12.6, 12.8, 13.0, 13.2, 13.4, 13.6, 13.8, 14.0, 14.2, 14.4, 14.6, 14.8, 15.0]
## Velocity Array
velocity = [30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 30.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -60.0, -60.0, -60.0, -60.0, -60.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 6.0, 12.0, 18.0, 24.0, 30.0, 24.0, 18.0, 12.0, 6.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
## Position Array
position = [0.0, 36.0, 72.0, 108.0, 144.0, 180.0, 216.0, 252.0, 288.0, 324.0, 360.0, 396.0, 432.0, 468.0, 504.0, 540.0, 576.0, 612.0, 648.0, 684.0, 720.0, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 720.09, 648.27, 576.27, 504.27, 432.27, 360.27, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 360.09, 363.69, 374.49, 392.49, 417.69, 450.09, 482.49, 507.69, 525.69, 536.49, 540.09, 540.09, 540.09, 540.09, 540.09, 540.09, 540.09, 540.09, 540.09, 540.09, 540.09]
