'''
@file Lab4_UI_Front.py
@brief This file serves as the Front End User Interface living on users local machine
@author Jacob Lindberg
@date October 22, 2020
@image html FSMDiagramLab4.PNG
@image html Lab4Plot.PNG

This file serves to act as the front end user interface to execute the encoder scripts on the
Nucleo Microcontroller. This program will prompt the user to initialize the main script to run 
on the microcontroller. Once running, data collection will be aggregated into an array with their 
respective timestamp values. If the command is recieved from the user to terminate the program, a
plot will be generated using the built in matplotlib module along with a .csv file containing
the generated data array retrieved from the microprocessor.
'''

import matplotlib.pyplot as plt
import serial
import time
import keyboard
import numpy as np 

ser = serial.Serial(port='COM3', baudrate = 115273, timeout=1)

cmd = ''

while cmd != 'G':
    cmd = input("Press 'G' to start the data collection: ")
    if cmd == 'G':
        ser.write(cmd.encode('ascii'))
        break
    else:
        pass
## Empty Time Array    
t = []
## Empty Position Array
deg = []
## Initialize the time
start = round(time.time()*1000)
## Compute the end time
end = round(time.time()*1000) + 10E3
## While Loop for the Program Execution
while round(time.time()*1000) < end:
    print("The program has run for " + str((round(time.time()*1000) - start)/1000 ) + (" seconds"))
    if keyboard.is_pressed('S'):
        ser.write('S'.encode('ascii'))
        break
    else:
        val = ser.readline().decode('ascii')
        val.strip()
        data = val.split(',')
        t.append(float(data[1]))
        deg.append(float(data[0]))
        

print(t)
print(deg)
    
    
    
print("The program has terminated")
## PLot the Graph
plt.plot(t, deg, 'r')
plt.ylabel('position(deg)')
plt.xlabel('time(s)')
plt.title('Encoder Position vs. Time')
plt.axis([0, 10, 0, 180])

my_array = np.asarray([t, deg])
np.savetxt('my_array.csv', my_array, delimiter=',')

ser.close()
        
    
