'''
@file Lab7_Main.py
@brief Main file used to create the constructors for the drivers
@author Jacob Lindberg
@date December 2, 2020
@image html PositionProfile.PNG
@image html VelocityProfile.PNG

Since the serial communication for my computer was not working, I was told to generate the
relevant information, regarding the Speed and Position profile for the motor once the task 
was complete, as well as compute and display the associated 'J' metric for the motor. 
Both arrays were then transfered into MatLab where the plots were generated.
'''
## Import Neccessary Modules
from Lab7_Run import FSM 
#from pyb import UART
import Lab7_shares as shares
## Constructors
task1 = FSM()
## While loop for executing the function
print("The proportional gain value used for this run is: " + str(shares.Kp_prime))
while True:
    ans = input("Press 'Y' to start the program: ")
    if ans == 'Y':
        task1.run()
        break
    else:
        pass

