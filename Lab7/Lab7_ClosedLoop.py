'''
@file Lab7_ClosedLoop.py
@brief Closed Loop File used to compute the pulse width percent that is then 
        passed to the MotorDriver.py file
@author Jacob Lindberg
@date December 2, 2020

This file serves to implement the given equation from Professor Charlie to compute 
the required pulse width percentage needed to make the system reduce the steady 
state error from the actual speed of the motor to the desired speed

'''

import Lab7_shares as shares
## Closed Loop L% Class
class ClosedLoop:
    
    def __init__(self):
        '''
        @brief initializes the constructor
        '''    
        self.timer = 10    

    ## Function to compute the pulse width of the signal
    def run(self, pos):
        '''
        @brief runs the calculation to get the percentage of PWM needed. The upper and
        lower limits of the PWM do not turn the motor off, like the velocity profile says it should,
        rather, it denotes its zero setpoint value to be the lower cutoff duty cycle percentage of
        30%. Conversely, the upper limit is capped at 70%.
        '''
        ## Overflow protection
        if pos >= 74:
            pos = 74
        else:
            pos = pos
        ## Calculate Duty Cycle
        L_per = round(self.get_Kp() * abs(shares.velocity[pos] - shares.omega_act))
        ## Upper limit
        if L_per >= 70: 
            L_per = 70
        ## Lower Limit
        elif L_per < 30:
            L_per = 30
        ## Pass
        else:
            pass        
        return L_per  
          
    ## Get_Kp Function   
    def get_Kp(self):
        '''
        @brief Gets the Kp from the user input parameter
        '''
        shares.Kp = shares.Kp_prime / 3.3
        return shares.Kp
        