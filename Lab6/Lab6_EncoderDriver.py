'''
@file Lab6_EncoderDriver.py
@brief Driver Task created to compute the actual speed Omega_act
@author Jacob Lindberg
@date November 24, 2020

This file serves to function as the Encoder Driver task, handling the bulk of the computation
in the task, such as computing the posititon delta to then devide by the time delta to then 
add to the current position as to avoid overflow of the encoder values.

'''


import pyb


## Create the Encoder Driver Class
class EncoderDriver:
    
    '''
    @brief Initialize the EncoderDriver Task
    @param current_pos current position of the encoder
            update updates position of the encoder
            delta the offset delta of the encoder
   '''
    
    
    ## Initialization Function
    def __init__(self):
        ## Set Up Tiemr 3
        self.timer = pyb.Timer(8)
        ## Specify the prescaler and the period value for the timer
        self.timer.init(prescaler = 0, period = 65535)
        ## Define Timer Channel 1 to Pin B6
        self.timer.channel(1, pin = pyb.Pin.cpu.C6, mode = pyb.Timer.ENC_AB)
        ## Define Timer Channel 2 to pin B7
        self.timer.channel(2, pin = pyb.Pin.cpu.C7, mode = pyb.Timer.ENC_AB)
        ## Initialize the encoder counter
        self.timer.counter()
        ## Define the delta value
        self.delta = 0
        ## Define the current position 
        self.current_pos = 0
        
    def Current(self):
        current_pos = self.timer.counter()
        return current_pos

        
    
