'''
@file HW1_Elevator.py
@image html StateTransitionDiagram.PNG

This file serves as an example implementation of a finite-state-machine using
Python. This will provide the motion of an elevator that goes between two 
floors based on whether or not proximity sensors and button inputs are pressed
The user has a button that they can press to either go up to level 2 or down
to level 1

Proximity sensors are placed at each of the floors so that the elevator knows 
when to kill the motor.

'''

import time
from random import choice

## Elevator Class
class TaskElevator:
    '''
    @brief      A finite state machine to control an elevators motion between two floors
    @details    This class implements a finite state machine to control the
                operation of the elevator
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN = 1
    
    ## Constant defining State 2
    S2_MOVING_UP = 2
    
    ## Constant defining State 3
    S3_STOP_FLOOR_2 = 3
    
    ## Constant defining State 4
    S4_STOP_FLOOR_1 = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING = 5
    
    ## Default the value of the first floor button to false
    button_1 = False
    
    ## Default the value of the second floor button to false
    button_2 = False
    ## Initialization Function
    def __init__(self, interval, Motor, button_1, button_2, first, second):
        '''
        @brief      Creates the Elevator Object.
        @param interval The specified interval for the elevator task function
                Motor The motor used to control the elevator
                button_1 First floor button
                button_2 Second floor button
                first First floor proximity sensor
                second Second floor proximity sensor
        
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## The button onject used for the first floor button object
        self.button_1 = button_1
        
        ## The button object used for the second floor button limit
        self.button_2 = button_2
        
        ## The button object used for the first floor sensor limit
        self.first = first
        
        ## The button object used for the second floor sensor limit
        self.second = second
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    ## Run function
    def run(self):
        '''
        @brief      Runs one iteration of the task for the Elevator Movement
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT): # Initialization Stage
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                
                ## Automatically make the elevator move down
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Down()
                print("The Elvator is moving down to floor 1.")
            
            elif(self.state == self.S4_STOP_FLOOR_1):
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                
                if self.runs*self.interval > 2:
                    self.transitionTo(self.S5_DO_NOTHING)
                
                ## If the second floor button is pressed and the first floor proximity sensor is True, continue
                
                elif self.button_2.getButtonState():
                    print("The second floor button has been pressed")
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Up()
                    print("The Elevator is moving up to floor 2.")
            
            elif(self.state == self.S2_MOVING_UP):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                self.transitionTo(self.S3_STOP_FLOOR_2)
                self.Motor.Stop()
            
            elif(self.state == self.S3_STOP_FLOOR_2):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.button_1.getButtonState():
                    print("The first floor button has been pressed.")
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Down()
                    print("The Elevator is moving down to floor 1.")
            
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.first.getButtonState():
                    self.transitionTo(self.S4_STOP_FLOOR_1)
                    self.Motor.Stop()
                    
            elif(self.state == self.S5_DO_NOTHING):
                print(str(self.runs) + ' State 5 {:0.2f}'.format(self.curr_time - self.start_time))
                
            
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    ## Transition function
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
## Button Class        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents the random selection of the first and 
                second floor buttons within the elevator by our hypothetical
                user. 
    '''
    ## Initialization Function
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    ## Get Button State function
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    A randomized true or false value will be generated to 
                    serve in place of modeling the behavior of the typical
                    elevator user
        @return     A boolean representing the state of the button.
        '''
        
                
        return choice([True, False])
## Motor Driver Class
class MotorDriver:
    '''
    @brief      A motor driver for the elevator.
    @details    This class represents a motor driver used to control the 
                locomotion of our elevator
    '''
    ## Initialization function
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object for the Elevator
        '''
        pass
    ## Elevator Up Function
    def Up(self):
        '''
        @brief Motor is moving the elevator up
        
        '''
        self.motor = 1
        #print('Elevator is moving up')
    ## Elevator Down Function
    def Down(self):
        '''
        @brief Motor is moving the elevator down
        '''
        self.motor = 2
        #print('Elevator is moving down')
    ## Elevator Stop Function
    def Stop(self):
        '''
        @brief Motor is suspending the elevator
        '''
        self.motor = 0
        print('Elevator has stopped and is waiting for button to be pressed')

## Defining The Constructor:
Motor = MotorDriver()
button_1 = Button('PB6')
button_2 = Button('PB7')
first = Button('PB8')
second = Button('PB9')

## Task 1      
task1 = TaskElevator(0.1, Motor, button_1, button_2, first, second)
## Task 2
task2 = TaskElevator(0.3, Motor, button_1, button_2, first, second)

# To run the task call task1.run() over and over
## Run the Objects Created
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()

