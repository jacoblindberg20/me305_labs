'''
@file Lab2_main.py
@details This is the file that is run to execute the LED_Blinker Finite State
Machine

@author Jacob Lindberg
@date OCtober 12, 2020

'''

from Lab2_mytest import LED_Blinker
import pyb

# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor


## Creating a task object using the button and motor objects above
real_LED = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## Create the task
task1 = LED_Blinker(.000002, real_LED)

while True:
    task1.run()