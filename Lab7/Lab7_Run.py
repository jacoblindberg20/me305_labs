'''
@file Lab6_Run.py
@brief This file serves as the back end program for the Finite State Machine
@author Jacob Lindberg
@date November 12, 2020

This file is what links the feedback system together to get the closed loop 
proportional control. This program is also responsible for the timing of the 
system with regards to intervals and finite state machine stages. This program
handles the bulk of the computationa all within one task so as to speed up motor
response and feedback communication. Zero values are not set at zero, but are set 
to a minimum PWM percentage associated with the values that dont cause the motor
to saturate between commands.

'''

## Import Neccessary Modules
import utime
import Lab7_shares as shares 
from Lab7_MotorDriver import MotorDriver
from Lab7_EncoderDriver import EncoderDriver
from Lab7_ClosedLoop import ClosedLoop
from pyb import UART

## Finite State Machine Class
class FSM:
    
    ## State 0
    S0_INIT = 0
    ## State 1
    S1_USERPROMPT = 1
    ## State 2
    S2_MOTOR = 2
    ## State 3
    S3_ENCODER = 3
    ## State 4
    S4_STOP = 4
    
    ## Initialization Function
    def __init__(self):
        '''
        @brief initializes the program to run the closed loop task for the FSM
        '''
        ## Specify the start time of the program
        self.start_time = utime.ticks_ms()
        ## Specify the end time of the program
        self.end_time = int(shares.time[-1]) * 1000 + self.start_time
        ## Designate the initial state value
        self.state = 0
        ## Interval designated for time step calculations
        self.interval = shares.time_delta
        ## Encoder Position Array (Ticks)
        self.EncoderPositions = [0, 0]
        ## Encoder Position Array (Degrees)
        self.EncoderDegrees = [0]
        ## Encoder Speed Array (RPM)
        self.EncoderSpeeds = [0]
        ## Timestamp array in seconds
        self.TimeStamps = [0]
        ## Pulses per Revolution of the Encoder
        self.EncoderPPR = 4000
        ## Specify the UART Line
        self.uart = UART(2)
        ## Last Encoder Position
        self.EncoderLast = 0
        ## Current Encoder Position
        self.EncoderCurr = 0
        
    
    ## Run Function    
    def run(self):
        '''
        @brief runs the closed loop within the FSM
        '''
        
        self.curr_time = utime.ticks_ms()  
        if (self.curr_time <= self.end_time):
            if(self.state == self.S0_INIT): # Initialization Stage
                self.transitionTo(self.S1_USERPROMPT)
            elif(self.state == self.S1_USERPROMPT): 
                if shares.Kp_prime == True:
                    self.transitionTo(self.S2_MOTOR)
                else:
                    pass
            ## Specify Adder
            i = 0  
            ## Specify increments for averaged data arrays to ensure no data overflow
            j = 200
            ## While loop for task iterations
            while i <= (int(shares.time[-1]) * 1000)/self.interval:               
                               
                self.curr_time = utime.ticks_ms()
                if (self.curr_time) >= (self.start_time) + (self.interval*i):
                    pos = round((self.curr_time - self.start_time)/200)
                    if i == 0:
                        ## Call Motor Function to Spin Motor
                        MotorDriverTask.Motor1_GO(ClosedLoopTask.run(pos))
                        self.EncoderPositions[0] = EncoderDriverTask.Current()
                        self.TimeStamps.append(0)
                    elif i > 0:
                        MotorDriverTask.Motor1_GO(ClosedLoopTask.run(pos))
                        self.EncoderPositions[1] = EncoderDriverTask.Current()
                        if self.EncoderPositions[1] >= self.EncoderPositions[0]:
                            ## Distnace in revolutions
                            distance = ((self.EncoderPositions[1] - self.EncoderPositions[0]))/4000                                                      
                        elif self.EncoderPositions[1] < self.EncoderPositions[0]:
                            ## Distance in revolutions
                            distance = (((65535 - self.EncoderPositions[0]) + self.EncoderPositions[1]))/4000
                        ## Call Array Positioning once every 200 miliseconds
                        if (pos*200) >= j:
                            ## Array holding position profile in degrees
                            self.EncoderDegrees.append(round(((distance*360) * 720) / (185)))
                            ## Array holding velocity profile in RPM
                            self.EncoderSpeeds.append((((shares.omega_act - 7) * (60)) / (17)))
                            j += 200
                        else:
                            pass                         
                        ## Speed in Revolutions Per Minute
                        shares.omega_act = round((distance)/(self.interval/1E3))
                        k = pos
                        if k > 74:
                            k = 74
                        else:
                            pass
                        print("Degrees_Act: " + str(distance*360) + " Degrees_Ref: " + str(shares.position[k]) + "Current Iteration: " + str(i))
                        print("RPM_Act: " + str(shares.omega_act) + " RPM_Ref: " + str(shares.velocity[k]) + "Current Time: " + str((self.curr_time - self.start_time)/1E3))
                        self.EncoderPositions[0] = self.EncoderPositions[1]
                    ## Iterative Time Variable
                    i += 1
                    ## Iterative Position Variable for Array Classification
                    pos += 1
                else:
                    pass
            
            self.transitionTo(self.S4_STOP)   
            
            shares.prompt = False
            ## Stop the Motor and the Program
            MotorDriverTask.Motor1_STOP()
            sums = []
            if len(self.EncoderDegrees) == len(shares.position) and len(self.EncoderSpeeds) == len(shares.velocity):
                for i in range(len(shares.position)):
                    sums.append(round((int(shares.velocity[i]) - self.EncoderSpeeds[i])**2 + (int(shares.position[i]) - self.EncoderDegrees[i])**2))
                J = sum(sums)
                print("The Performance Metric Is: " + str(J))
                print("The reason it is so high is due to lots of room for error. PWM values are not cut at zero but at a threshold voltage and are not inverted to be a negative number. This accounts for alot of it.")
            else:
                print("Please Run The Program Again to Calculate J. Instances Were Skipped...")
            
    ## Transition function
    def transitionTo(self, newState):
        '''
        @brief Updates the variable defining the next state to run
        '''
        self.state = newState
        
        
## BUILD THE CONSTRUCTORS
MotorDriverTask = MotorDriver()
EncoderDriverTask = EncoderDriver()
ClosedLoopTask = ClosedLoop()
   
        
        
        