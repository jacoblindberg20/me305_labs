
'''
@file Lab3_shares.py
@brief A container for all the inter-task variables
@author Jacob Lindberg
@date October 20, 2020

This is the file that shares the common data aggregates for both Encoder and
EncoderDriver programs to reference. See the FSM_Lab3.PNG image for further
clarification.

'''
pos = 0

delta = None
