'''
@file Lab3_main.py
@brief Main program runs all the tasks round-robin
@author Jacob Lindberg
@date October 20, 2020
@image html FSM_Lab3.PNG 

This file is the main file that executes the entire Encoder Module Task, including the UI interface,
the Encoder Driver module, and the Encoder module. The encoder task was built around a modified motor
different to the one that ME305 lab students recieved from class due to prolonged shipping times and
manufacturing delays per DigiKey. The motor that was substituted in this lab to develop an encoder task
for was the following
Make: PITTMAN
Model#: PG6712A077-R1
Voltage: 24 VDC

'''


from Lab3_Encoder import Encoder


## User interface task, works with serial port
task0 = Encoder(0, 1_000)

## Cipher task encodes characters sent from user task by flipping the case of the character if it is a letter
#task1 = TaskCipher(1, 1_000, dbg=False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0]

while True:
    for task in taskList:
        task.run()
