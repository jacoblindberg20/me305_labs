# Fibonacci Sequence

'''
@file Lab1_fibonacci_sequence.py
@brief the fibonacci sequence generator with user input prompt

@author Jacob Lindberg
@date September 28, 2020

'''

while True:
    '''
    @brief While loop that prompts user about inputing an index value
    @details This while loop will iterate endlessly until a valid input is accepted
    '''
    try:
        ## @brief this val is the bin where our index value is placed
        val = int(input("Please enter an index value: "))
        if val >= 0:
            break
        else:
            continue
    except ValueError:
        print("Please enter a valid integer!")

            
        
# Build open array to populate
##This open array serves as the list that the sequence will be populated into
sequence = []
## fibonacci Sequence Generator
def fib_sequence(val): #this is a method
    '''
    @brief this function will calculate the last value of the fibonacci sequence
    @param val A integer that resembles the desired indexed value of the sequence
    '''
    ##This value sets the first integer of the fibonacci sequence
    value = 0 # Specify the first value of the sequence
    ##This index specifies the resultant
    index = 1 # Specify the resultant
    for i in range(val):
        sequence.append(value)
        value = index
        if i > 0:
            index = sequence[i] + value
        else:
            index = 1
    x = sequence[-1] # Take last value of the sequence
    return x

print("The fibonacci sequence is " + str(fib_sequence(val)))
    

